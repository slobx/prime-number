package com.slobx.www.primenumber;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    EditText numberField;
    Button resultButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        numberField = (EditText) findViewById(R.id.etNumber);
        resultButton = (Button) findViewById(R.id.resultButton);
    }

    private boolean isPrime(int number) {
        int i;
        for (i = 2; i <= number / 2; i++) {
            if (number % i == 0) {
                return false;
            }
        }
        return true;
    }

    public void calculateResult(View view){
        String inputText = numberField.getText().toString();
        int number = Integer.parseInt(inputText);
        if (inputText.matches("")) {
            makeToast("You must enter a number");
        } else {
            boolean result = isPrime(number);
            if (result){
                makeToast("Number " + number + " is a prime number");
                numberField.setText("");
            } else {
                makeToast("Sorry but number " + number + " is not a prime number");
                numberField.setText("");
            }

        }
    }

    private void makeToast(String toastText) {
        Toast.makeText(getApplicationContext(), toastText, Toast.LENGTH_SHORT).show();
    }
}
